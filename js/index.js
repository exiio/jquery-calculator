// Write your code here
$(document).ready(function() {
    let result = 0;
    let prevEntry = 0;
    let operation = null;
    let currentEntry = '0';
    let displayEntry = '0';
    let storeArr = [];
    let itr = 0;
    updateScreen(result);
    
    $('.button').on('click', function() {
      let buttonPressed = $(this).html();
     
      if (buttonPressed === 'C') {
        result = 0;
        currentEntry = '0';
        displayEntry = '0';
        storeArr = [];
        updateScreen(displayEntry);
      } else if (buttonPressed === '.') {
        currentEntry += '.';
        displayEntry += '.';
        updateScreen(displayEntry);
      } else if (isNumber(buttonPressed)) {
        if (currentEntry === '0') {
            currentEntry = buttonPressed;
            displayEntry = buttonPressed;
            updateScreen(displayEntry);
        } else {
            currentEntry = currentEntry + buttonPressed;
            if (operation !== null) {
                displayEntry = displayEntry + currentEntry;
            } else {
                displayEntry = displayEntry + buttonPressed;
            }
            updateScreen(displayEntry);
        }
      } else if (isOperator(buttonPressed)) { 
        storeArr.push(currentEntry);
        prevEntry = parseFloat(currentEntry);
        operation = buttonPressed;
        displayEntry = currentEntry + buttonPressed;
        updateScreen(displayEntry);
        //currentEntry = '';
        if (isOperator(displayEntry)) {
            operation = displayEntry;
            prevEntry = storeArr[0];
            displayEntry = prevEntry;
            updateScreen(displayEntry);
        }
        currentEntry = '';
      } else if (buttonPressed === '=') {
        itr = itr + 1;
        currentEntry = operate(prevEntry, currentEntry, operation, itr);
        if (currentEntry === Infinity) {
            currentEntry = 'ERROR';
            $(`.log-container${itr}`).html('');
        } 
        updateScreen(currentEntry);
        operation = null;
      }
    });
  });
  
  let updateScreen = function(displayValue) {
    let lastResult = displayValue.toString();
    $('#display').html(lastResult);
  };

  let isNumber = function(value) {
    return !isNaN(value);
  }

  let isOperator = function(value) {
    return value === '/' || value === '*' || value === '+' || value === '-';
  };
  let logFunc = function(logValue, itr) {
    let newElem = $(`<div class="log-container log-container${itr}">
    <div class="circle circle${itr}"></div><div class='log-message log-message${itr}'>${logValue}</div>
    <div class="cross cross${itr}">✖</div></div>`)
    $('.log').prepend(newElem);
    let checkText = $(`.log-message${itr}`).text();
    let a = '48';
    if (checkText.indexOf(a) > -1) {
        $(`.log-message${itr}`).addClass('underline');
    }
    $(`.underline`).css({'text-decoration': 'underline'});

    $(`.circle${itr}`).click(function(){
        $(`.circle${itr}`).toggleClass('red');
    })
    $(`.cross${itr}`).click(function(){
        $(`.log-container${itr}`).remove();
    })
    $(`.cross${itr}`).hover(function(){
        $(`.cross${itr}`).toggleClass('cross-red');
    })
    $('.log').scroll(function(){
        console.log('Scroll Top:', $(`.log`).scrollTop());
    })
  }
  let operate = function(a, b, operation, itr) {
    a = parseFloat(a);
    b = parseFloat(b);
    if (operation === '+') {
        let lorResult = a + b;
        let strLog = a + operation + b + '=' + lorResult;
        logFunc(strLog, itr);
        return lorResult
    }
    if (operation === '-') {
        let lorResult = a - b;
        let strLog = a + operation + b + '=' + lorResult;
        logFunc(strLog, itr);
        return lorResult
    }
    if (operation === '*') {
        let lorResult = a * b;
        let strLog = a + operation + b + '=' + lorResult;
        logFunc(strLog, itr);
        return lorResult
    }
    if (operation === '/') {
        let lorResult = a / b;
        let strLog = a + operation + b + '=' + lorResult;
        logFunc(strLog, itr);
        return lorResult
    }
  }
